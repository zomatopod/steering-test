
class Kinematic {
	PVector position = new PVector(0,0);
	PVector velocity = new PVector(0,0);
	float orientation = 0;
	float rotation = 0;
	float maxSpeed = 7;
	float maxRotation = 0.06;

	Steering steer;

	Kinematic(Steering s) {
		steer = s;
	}

	Kinematic() {
		this(null);
	}

	void SetSteering(Steering s) {
		steer = s;
	}

	void SetHeading(float h) {
		float m = velocity.mag();

		velocity.x = cos(h);
		velocity.y = sin(h);
		velocity.mult(m);
	}

	float GetHeading() {
		return atan2(velocity.y, velocity.x);
	}

	void Update() {
		if (steer != null) {
			steer.Steer();
			velocity.add(steer.linear);
			rotation += steer.angular;

			position.add(velocity);
			orientation += rotation;

			if (velocity.mag() > maxSpeed) {
				velocity.normalize();
				velocity.mult(maxSpeed);
			}

			float aRotation = abs(rotation);
			if (rotation > maxRotation) {
				rotation /= aRotation;
				rotation *= maxRotation;
			}

			steer.DebugDraw();
		}
	}

}

class Steering {
	PVector linear = new PVector(0,0);
	float angular = 0;

	Steering() {
	}

	void Steer() {
	}

	void DebugDraw() {
	}
}

class Seek extends Steering {
	Kinematic source;
	Kinematic target;
	float maxAccel = 0.3;

	Seek(Kinematic s, Kinematic t) {
		source = s;
		target = t;
	}

	void Steer() {
		linear = PVector.sub(target.position, source.position);
		linear.normalize();
		linear.mult(maxAccel);

		angular = 0;
	}
}

class Arrive extends Seek {
	float targetRadius = 10;
	float slowRadius = 100.0;
	float timeToTarget = 10.0;

	Arrive (Kinematic s, Kinematic t) {
		super(s, t);
	}

	void Steer() {
		PVector direction = PVector.sub(target.position, source.position);

		float distance = direction.mag();
		float speed = 0.0;

		// At target
		if (distance < targetRadius) {
			linear.x = linear.y = 0.0;
			return;
		}

		// Slow down if inside the slow radius
		if (distance < slowRadius) {
			speed = source.maxSpeed * distance / slowRadius;
		} else {
			speed = source.maxSpeed;
		}

		PVector targetVelocity = direction;
		targetVelocity.normalize();
		targetVelocity.mult(speed);

		// Accelerate to target velocity
		linear = PVector.sub(targetVelocity, source.velocity);
		linear.div(timeToTarget);

		// Clamp to max acceleration
		if (linear.mag() > maxAccel) {
			linear.normalize();
			linear.mult(maxAccel);
		}

		source.orientation = atan2(source.velocity.y, source.velocity.x);
		angular = 0;
	}
}

class Pursue extends Seek {
	Kinematic realTarget = new  Kinematic();
	float maxPrediction = 20;

	Pursue(Kinematic s, Kinematic t) {
		super(s, t);
		realTarget = t;
	}

	void Steer() {
		Kinematic k = new Kinematic();

		PVector direction = PVector.sub(realTarget.position, source.position);

		float distance = direction.mag();
		float speed = source.velocity.mag();
		float prediction;

		// if speed is too slow to make good prediction
		if (speed < distance / maxPrediction) {
			prediction = maxPrediction;
		} else {
			prediction = distance / speed;
		}

		PVector p = PVector.mult(realTarget.velocity, prediction);

		k.position = realTarget.position.get();
		k.position.add(p);

		target = k;
		super.Steer();
	}
}

class FollowPath extends Arrive {
	Path path;
	float pathOffset = 120;

	FollowPath(Kinematic s, Path p) {
		super(s, s);
		path = p;
	}

	void Steer() {
		Kinematic k = new Kinematic();
		float currentParam = path.GetParameter(source.position, 0);
		float targetParam = currentParam + pathOffset;

		k.position = path.GetPosition(targetParam);

		target = k;
		super.Steer();
	}

	void DebugDraw() {
		PVector nearest = path.GetPosition(path.GetParameter(source.position, 0));
		PVector st = target.position;

		fill(#ffff00);
		ellipse(nearest.x, nearest.y, 10, 10);
		fill(#ff002f);
		ellipse(st.x, st.y, 10, 10);
	}
}

class Align extends Steering {
	Kinematic source;
	Kinematic target;

	float maxAngularAccel = 0.6;
	float timeToTarget = 1.0;

	float targetRadius = 0.01;
	float slowRadius = 0.6;

	Align(Kinematic s, Kinematic t) {
		source = s;
		target = t;
	}

	void Steer() {
		float targetRotation = 0.0;

		float rotationDelta = target.orientation - source.orientation;
		rotationDelta = mapToRange(rotationDelta);

		float rotationDeltaSize = abs(rotationDelta);

		// At target
		if (rotationDeltaSize < targetRadius) {
			targetRotation = 0.0;
			return;
		}

		// Slow down if inside the slow radius
		if (rotationDeltaSize < slowRadius) {
			targetRotation = source.maxRotation * rotationDeltaSize / slowRadius;
		} else {
			targetRotation = source.maxRotation;
		}

		// Flip back to correct sign
		targetRotation = targetRotation * rotationDelta / rotationDeltaSize;

		// Accelerate to target rotation
		angular = targetRotation - source.rotation;

		// Clamp to max acceleration
		float angularAcceleration = abs(angular);
		if (angularAcceleration > maxAngularAccel) {
			angular /= angularAcceleration;
			angular *= maxAngularAccel;
		}
	}

	//can be done better using modulus apparently. Look into this!!
	float mapToRange(float value) {
		float r = value;

		if(value > PI) {
			r -= 2*PI;
			r = mapToRange(r);
		} else if(value < -PI) {
			r += 2*PI;
			r = mapToRange(r);
		}

		return r;
	}
}

class Face extends Align {
	Kinematic realTarget = new Kinematic();

	Face(Kinematic s, Kinematic t) {
		super(s, t);
		realTarget = t;
	}

	void Steer() {
		Kinematic k = new Kinematic();
		k.orientation = atan2(realTarget.position.y - source.position.y, realTarget.position.x - source.position.x);
		target = k;

		super.Steer();
	}
}

class VelocityMatch extends Seek {
	float timeToTarget = 10.0;

	VelocityMatch (Kinematic s, Kinematic t) {
		super(s, t);
	}

	void Steer() {
		linear = PVector.sub(target.velocity, source.velocity);
		linear.div(timeToTarget);

		// Clamp to max acceleration
		if (linear.mag() > maxAccel) {
			linear.normalize();
			linear.mult(maxAccel);
		}

		source.orientation = atan2(source.velocity.y, source.velocity.x);
		angular = 0;
	}
}
