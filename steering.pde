interface Drawable {
	void Draw();
}

interface Updatable {
	void Update();
}

ArrayList objects;

void setup() {
	size(1024, 768);

	objects = new ArrayList();

	KinematicActor target = new MouseTarget();
	Mover seek = new Mover();
	Mover followPath = new Mover();
	Mover pursue = new Mover();

	Path path = new Path();
	path.points.add(new PVector(200, 100));
	path.points.add(new PVector(220, 300));
	path.points.add(new PVector(50, 400));
	path.points.add(new PVector(100, 500));
	path.points.add(new PVector(940, 700));
	path.points.add(new PVector(1000, 60));

	seek.kinematic.SetSteering(new Arrive(seek.kinematic, target.kinematic));
	followPath.kinematic.SetSteering(new FollowPath(followPath.kinematic, path));
	pursue.kinematic.SetSteering(new Pursue(pursue.kinematic, seek.kinematic));

	seek.kinematic.position.x = 150;
	seek.kinematic.position.y = 60;

	followPath.kinematic.position.x = 400;
	followPath.kinematic.position.y = 0;
	followPath.fillColor = #00ff00;

	pursue.kinematic.position.x = 0;
	pursue.kinematic.position.y = 880;
	pursue.kinematic.maxSpeed = 10;
	pursue.fillColor = #f01125;


	objects.add(seek);
	objects.add(followPath);
	objects.add(pursue);
	objects.add(target);
	objects.add(path);
}

void draw() {
	background(0);

	for (int i = 0; i < objects.size(); i ++) {
		Object o = objects.get(i);

		stroke(#000000);
		fill(#ffffff);

		if (o instanceof Updatable) {
			((Updatable)o).Update();
		}

		if (o instanceof Drawable) {
			((Drawable)o).Draw();
		}
	}
}