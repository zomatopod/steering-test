class Path implements Drawable {
	ArrayList points = new ArrayList();

	float GetParameter(PVector pos, float lastParam) {
		float parameter = 0;
		float bestDistance = -1;
		float pathLength = 0;
		PVector segmentStart;
		PVector segmentEnd;

		if (points.size() > 0) {
			// Iterate through the path's segments to find the parameter

			segmentStart = (PVector)points.get(0);

			for (int i = 1; i < points.size(); i++) {
				PVector nearestSegmentPoint;
				float distance = 0;
				float segmentLength = 0;

				segmentEnd = (PVector)points.get(i);
				segmentLength = PVector.sub(segmentEnd, segmentStart).mag();

				// We want to find the point on the path that's closest to the actor, and then calc the parameter for it.
				// Parameter is the distance along the path from the start to said point.

				nearestSegmentPoint = FindNearestPointOnSegment(segmentStart, segmentEnd, pos);
				distance = PVector.sub(nearestSegmentPoint, pos).mag();

				if (distance < bestDistance || bestDistance == -1) {
					bestDistance = distance;
					parameter = pathLength + PVector.sub(nearestSegmentPoint, segmentStart).mag();
				}

				pathLength += segmentLength;
				segmentStart = segmentEnd;
			}
		}

		return parameter;
	}

	PVector GetPosition(float parameter) {
		PVector position = new PVector();
		float distance = parameter;

		if (points.size() > 0) {
			position = ((PVector)points.get(0)).get();

			for (int i = 1; i < points.size(); i++) {
				PVector end = (PVector)points.get(i);
				PVector d = PVector.sub(end, position);

				if (d.mag() > distance) {
					d.mult(distance / d.mag());
					position.add(d);
					break;
				} else {
					distance -= d.mag();
					position = end.get();
				}
			}
		}

		return position;
	}

	PVector FindNearestPointOnSegment(PVector pt1, PVector pt2, PVector testPoint) {
		PVector p = new PVector();

 		float dx = pt2.x - pt1.x;
		float dy = pt2.y - pt1.y;
		float t = ((testPoint.x - pt1.x) * dx + (testPoint.y - pt1.y) * dy) / (dx * dx + dy * dy);
		p.x = pt1.x + (t * dx);
		p.y = pt1.y + (t * dy);

		// Clamp to line segment
		if (t < 0.0) {			// Beyond start of segment
			p = pt1.get();
		} else if (t > 1.0) {	// Beyond end of segment
			p = pt2.get();
		}

		return p;
	}


	void Draw() {
		PVector segmentStart;
		PVector segmentEnd;
		PVector p;

		if (points.size() > 0) {
			stroke(#ffffff);

			segmentStart = (PVector)points.get(0);
			for (int i = 1; i < points.size(); i++) {
				segmentEnd = (PVector)points.get(i);
				line(segmentStart.x, segmentStart.y, segmentEnd.x, segmentEnd.y);
				segmentStart = segmentEnd;
			}

			for (int x = 0; x < points.size(); x++) {
				p = (PVector)points.get(x);
				ellipse(p.x, p.y, 5, 5);
			}
		}
	}
}