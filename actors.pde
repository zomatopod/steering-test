class Actor implements Drawable, Updatable {
	Actor() {
	}

	void Update() {}
	void Draw() {}
}

class KinematicActor extends Actor {
	Kinematic kinematic = new Kinematic();

	KinematicActor() {
	}
}

class Target extends KinematicActor {
	Target(float x, float y) {
		kinematic.position.x = x;
		kinematic.position.y = y;
	}

	void Draw() {
		pushMatrix();
		translate(kinematic.position.x, kinematic.position.y);
		fill(#ffffff);
		ellipse(0, 0, 15, 15);
		fill(#000000);
		ellipse(0, 0, 5, 5);
		popMatrix();
	}
}

class MouseTarget extends Target {
	MouseTarget() {
		super(mouseX, mouseY);
	}

	void Update() {
		kinematic.position.x = mouseX;
		kinematic.position.y = mouseY;
	}
}

class Mover extends KinematicActor {
	color fillColor = #ffffff;

	Mover() {
	}

	void Update() {
		OnUpdate();
		kinematic.Update();
	}

	void Draw() {
		pushMatrix();

		translate(kinematic.position.x, kinematic.position.y);
		rotate(kinematic.GetHeading());
		// rotate(kinematic.orientation);
		OnDraw();

		popMatrix();
	}

	void OnUpdate() {
	}

	void OnDraw() {
		// Iso triangle pointing to the right (heading 0)
		fill(fillColor);
		triangle(20, 0, -20, -10, -20, 10);
	}
}